# Work-Env

## Usage
Use docker-compose to create a working environment, like a vm that you can login to use, and easy to restore your environment.  
you can use info below to pull the image or build your self by the dockerfile.  
```
Image: registry.gitlab.com/zealot0515/work-env:env-2022-06-20
user: pull_user
pwd: Tyj-Vth4MtZVx9mNe8xY

docker login registry.gitlab.com/zealot0515/work-env:env-2022-06-20 -u pull_user -p Tyj-Vth4MtZVx9mNe8xY
```
## docker-compose
```
version: '2'

services:
    work-env:
        image: registry.gitlab.com/zealot0515/work-env:env-2022-07-12
        container_name: work-env
        hostname: work-env
        restart: always 
        volumes:
            - "./.ssh/:/home/ubuntu/.ssh:ro"   
            - "./sync/:/home/ubuntu/sync:rw"
            - "./go/:/home/ubuntu/go:rw"
            - "/var/run/docker.sock:/var/run/docker.sock:rw"
            - "./.kube/:/home/ubuntu/.kube"
        ports:
            - "59922:22"
        command: 'bash -c "
   sudo chmod 666 /var/run/docker.sock
&& sudo -u ubuntu git config --global user.email \"pagi.cheng@opennet.tw\"
&& sudo -u ubuntu git config --global user.name \"pagi.cheng\"
&& sudo -u ubuntu git config --global core.fileMode false
&& sudo -u ubuntu git config --global --add safe.directory /home/ubuntu/
&& sudo -u ubuntu git config --global pull.rebase true 
&& sudo -u ubuntu git config --global credential.helper \"store\"
&&            /usr/sbin/sshd -D
                "
            '

```
and you can mount any folder you like, (.ssh is necessary).  
* .ssh.  
 in .ssh, authorized_keys can let you using key to login the container environment by ssh.   
 .ssh/ssh-add folder you can put some private key, it will be auto load to ssh-agent when you login.   

