FROM ubuntu:20.04
# change time zone
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive TZ=Asia/Taipei apt-get -y install tzdata

# Install service 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y sudo libterm-readkey-perl nodejs npm dnsutils vim net-tools wget unzip nano tree telnet openssh* curl git python3 python3.10 python2 python3-distutils python3-apt software-properties-common jq openvpn vim fish netcat iftop iputils-ping --fix-missing

# Install Ansible
RUN cd /tmp/ && wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py && rm get-pip.py
RUN pip3 install ansible==2.9.27
RUN ansible --version

# Install Jinja-Cli
RUN pip3 install jinja-cli

# docker
RUN curl -fsSL https://get.docker.com -o /tmp/get-docker.sh && bash /tmp/get-docker.sh && rm /tmp/get-docker.sh
RUN curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

# install terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install terraform

# install git lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash 
RUN apt-get update &&  DEBIAN_FRONTEND=noninteractive apt-get install git-lfs

# install aws cli
RUN mkdir /tmp/awscli/ && cd /tmp/awscli/ && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install && cd / && rm /tmp/awscli -rf

# user
RUN useradd -ms /bin/bash ubuntu
RUN usermod -aG docker ubuntu

# install go
RUN add-apt-repository ppa:longsleep/golang-backports
RUN apt update
RUN apt -y install golang-go
# create go folders
RUN mkdir -p /home/ubuntu/go;
RUN go version

# Install SOPS 3.7.1
RUN wget -qO /usr/local/bin/sops https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux && chmod a+x /usr/local/bin/sops

# Install Mongo Shell
RUN mkdir /tmp/mongoshell/ && cd /tmp/mongoshell/ && wget "https://fastdl.mongodb.org/linux/mongodb-shell-linux-x86_64-ubuntu2004-5.0.9.tgz" && tar zxf mongodb-shell-linux-x86_64-ubuntu2004-5.0.9.tgz && mv mongodb-linux-x86_64-ubuntu2004-5.0.9/bin/mongo /usr/local/bin

# Install YQ 4.30.6
RUN wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.30.6/yq_linux_amd64 && chmod a+x /usr/local/bin/yq

# Install wscat
RUN npm install -g wscat

# Install Kubectl
#RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && chmod +x kubectl && mv kubectl /usr/local/bin/
RUN curl -LO "https://dl.k8s.io/release/v1.21.14/bin/linux/amd64/kubectl" && chmod +x kubectl && mv kubectl /usr/local/bin/
# Helm
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
# Kubectx
RUN git clone https://github.com/ahmetb/kubectx /opt/kubectx
RUN ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
RUN ln -s /opt/kubectx/kubens /usr/local/bin/kubens

# K9s
RUN mkdir -p /tmp/k9s && cd /tmp/k9s && wget https://github.com/derailed/k9s/releases/download/v0.26.3/k9s_Linux_x86_64.tar.gz && tar zxf k9s_Linux_x86_64.tar.gz && mv k9s /bin/ && cd /tmp/ && rm /tmp/k9s -rf

# Start sshd service
RUN ssh-keygen -A
RUN mkdir /run/sshd
EXPOSE 22

# Install SAM
RUN cd /tmp; wget https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip; unzip aws-sam-cli-linux-x86_64.zip -d sam-installation ; RUN ./sam-installation/install ; sam --version ; rm -rf aws-sam-cli-linux-x86_64.zip sam-installation


# copy ubuntu prive keys to user .ssh and change owner
RUN chown -R ubuntu:ubuntu /home/ubuntu
RUN echo 'ubuntu ALL=(ALL) NOPASSWD:ALL' >> /etc/ers

# add bash_profile setting
RUN echo 'export GOPATH=$HOME/go\nexport HISTSIZE=10000\nexport HISEFILESIZE=10000\nexport PATH=$PATH:$GOPATH/bin\neval $(ssh-agent -s)\nssh-add ~/.ssh/ssh-add/*\n' >> /home/ubuntu/.bash_profile

VOLUME ["/sys/fs/cgroup"]
CMD ["/usr/sbin/sshd", "-D"]
